#!/bin/sh
while ! nc -z config-service 8888 ; do
    echo "Waiting for the Alor Config Service"
    sleep 3
done
while ! nc -z registry-service 8761 ; do
    echo "Waiting for the Alor Registry Service"
    sleep 3
done
java -Djava.security.egd=file:/dev/./urandom -jar /customer-service.jar